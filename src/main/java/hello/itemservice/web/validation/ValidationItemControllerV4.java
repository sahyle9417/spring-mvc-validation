package hello.itemservice.web.validation;

import hello.itemservice.domain.item.Item;
import hello.itemservice.domain.item.ItemRepository;
import hello.itemservice.web.validation.form.ItemSaveForm;
import hello.itemservice.web.validation.form.ItemUpdateForm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/validation/v4/items")
@RequiredArgsConstructor
public class ValidationItemControllerV4 {

    private final ItemRepository itemRepository;

    @GetMapping
    public String items(Model model) {
        List<Item> items = itemRepository.findAll();
        model.addAttribute("items", items);
        return "validation/v4/items";
    }

    @GetMapping("/{itemId}")
    public String item(@PathVariable long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "validation/v4/item";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        model.addAttribute("item", new Item());
        return "validation/v4/addForm";
    }

    // ModelAttribute 어노테이션은 각 필드에 대해 타입 변환 시도해서 실패하면 typeMismatch 로 FieldError 추가
    // 타입 변환에 성공한 필드에 대해서 BeanValidator 가 검증 시도 (타입 변환 실패한 필드는 BeanValidator 검증 수행 X)
    // BeanValidator 는 Validated 또는 Valid 어노테이션 붙은 객체의 각 필드에 대해
    // 검증 어노테이션(NotNull, NotBlank, Range, Max 등)에 맞게 검증 수행하고 실패 시 BindingResult 에 등록
    // BeanValidation 에러 메시지 우선순위 : properties 파일 -> 검증 어노테이션 message 속성 -> 라이브러리 기본값
    // ItemSaveForm 객체명을 form 으로 지정하되, 뷰에서 참조할 model 객체명으로는 item 을 사용하여 뷰는 수정하지 않아도 됨
    @PostMapping("/add")
    public String addItem(@Validated @ModelAttribute("item") ItemSaveForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        // FieldError 는 BeanValidator 와 어노테이션으로 쉽게 수행 가능하지만,
        // 아래와 같이 여러 Field 들을 종합적으로 보고 검증해야하는 ObjectError 는 사용자가 직접 아래와 같이 로직을 구현해야 함
        if (form.getPrice() != null && form.getQuantity() != null) {
            int resultPrice = form.getPrice() * form.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
                // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
                // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
                // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
                bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));
            }
        }

        if (bindingResult.hasErrors()) {
            return "validation/v4/addForm";
        }

        // ItemSaveForm 객체 검증 성공 시 저장할 Item 객체로 변환
        Item item = new Item();
        item.setItemName(form.getItemName());
        item.setPrice(form.getPrice());
        item.setQuantity(form.getQuantity());

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v4/items/{itemId}";
    }

    @GetMapping("/{itemId}/edit")
    public String editForm(@PathVariable Long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "validation/v4/editForm";
    }

    // ItemSaveForm 객체명을 form 으로 지정하되, 뷰에서 참조할 model 객체명으로는 item 을 사용하여 뷰는 수정하지 않아도 됨
    @PostMapping("/{itemId}/edit")
    public String edit(@PathVariable Long itemId, @Validated @ModelAttribute ItemUpdateForm form, BindingResult bindingResult) {

        // FieldError 는 BeanValidator 와 어노테이션으로 쉽게 수행 가능하지만,
        // 아래와 같이 여러 Field 들을 종합적으로 보고 검증해야하는 ObjectError 는 사용자가 직접 아래와 같이 로직을 구현해야 함
        if (form.getPrice() != null && form.getQuantity() != null) {
            int resultPrice = form.getPrice() * form.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
                // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
                // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
                // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
                bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));
            }
        }

        if (bindingResult.hasErrors()) {
            return "validation/v4/editForm";
        }

        // ItemSaveForm 객체 검증 성공 시 저장할 Item 객체로 변환
        Item itemParam = new Item();
        itemParam.setItemName(form.getItemName());
        itemParam.setPrice(form.getPrice());
        itemParam.setQuantity(form.getQuantity());

        itemRepository.update(itemId, itemParam);
        return "redirect:/validation/v4/items/{itemId}";
    }

}
