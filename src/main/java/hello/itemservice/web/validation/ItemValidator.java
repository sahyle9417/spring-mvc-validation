package hello.itemservice.web.validation;

import hello.itemservice.domain.item.Item;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ItemValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        // Parent.class.isAssignableFrom(Child) == true
        return Item.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Item item = (Item) target;

        // 검증 로직
        if (!StringUtils.hasText(item.getItemName())) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // bindingResult.addError(new FieldError("item", "itemName", item.getItemName(), false, new String[]{"required.item.itemName"}, null, null));

            // rejectValue(fieldName, errorCode)
            // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
            // 필드 에러 메시지 코드 우선순위 (에러코드.객체명.필드명 -> 에러코드.필드명 -> 에러코드.필드타입 -> 에러코드)
            // FieldError 객체의 codes 배열에 MessageCodesResolver 가 반환한 필드 에러 메시지 코드들을 담아서 FieldError 객체 생성 후 이를 bindingResult 에 담는다.
            errors.rejectValue("itemName", "required");
        }

        if (item.getPrice() == null || item.getPrice() < 1000 || item.getPrice() > 1000000) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // bindingResult.addError(new FieldError("item", "price", item.getPrice(), false, new String[]{"range.item.price"}, new Object[]{1000, 1000000}, null));

            // rejectValue(fieldName, errorCode, errorArgs, defaultMessage)
            // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
            // 필드 에러 메시지 코드 우선순위 (에러코드.객체명.필드명 -> 에러코드.필드명 -> 에러코드.필드타입 -> 에러코드)
            // FieldError 객체의 codes 배열에 MessageCodesResolver 가 반환한 필드 에러 메시지 코드들을 담아서 FieldError 객체 생성 후 이를 bindingResult 에 담는다.
            errors.rejectValue("price", "range", new Object[]{1000, 1000000}, null);
        }

        if (item.getQuantity() == null || item.getQuantity() >= 9999) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // bindingResult.addError(new FieldError("item", "quantity", item.getQuantity(), false, new String[]{"max.item.quantity"}, new Object[]{9999}, null));

            // rejectValue(fieldName, errorCode, errorArgs, defaultMessage)
            // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
            // 필드 에러 메시지 코드 우선순위 (에러코드.객체명.필드명 -> 에러코드.필드명 -> 에러코드.필드타입 -> 에러코드)
            // FieldError 객체의 codes 배열에 MessageCodesResolver 가 반환한 필드 에러 메시지 코드들을 담아서 FieldError 객체 생성 후 이를 bindingResult 에 담는다.
            errors.rejectValue("quantity", "max", new Object[]{9999}, null);
        }

        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));

                // reject(errorCode, errorArgs, defaultMessage)
                // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
                // 객체 에러 메시지 코드 우선순위 (에러코드.객체명 -> 에러코드)
                // ObjectError 객체의 codes 배열에 MessageCodesResolver 가 반환한 객체 에러 메시지 코드들을 담아서 ObjectError 객체 생성 후 이를 bindingResult 에 담는다.
                errors.reject("totalPriceMin", new Object[]{10000, resultPrice}, null);
            }
        }

    }
}
