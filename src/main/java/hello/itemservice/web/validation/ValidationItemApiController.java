package hello.itemservice.web.validation;

import hello.itemservice.web.validation.form.ItemSaveForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// ModelAttribute vs RequestBody 어노테이션

// ModelAttribute 어노테이션
// - 요청 파라미터 또는 form 데이터 처리
// - 필드 단위 검증 가능
// - 일부 필드 타입이 맞지 않아도 나머지 필드 Binding, controller 호출, Validator 검증 가능

// RequestBody 어노테이션
// - 요청 Body 데이터 처리
// - 필드 단위 검증 불가 (객체 단위 검증만 가능)
// - 일부 필드 타입이 맞지 않으면 전체 필드 Binding 실패 및 Validator 검증 불가

@Slf4j
@RestController
@RequestMapping("/validation/api/items")
public class ValidationItemApiController {

    @PostMapping("/add")
    public Object addItem(@RequestBody @Validated ItemSaveForm form, BindingResult bindingResult) {
        log.info("addItem API call");

        if (bindingResult.hasErrors()) {
            log.info("addItem API fail, errors = {}", bindingResult);
            return bindingResult.getAllErrors();
        }
        log.info("addItem API success");
        return form;
    }
}
