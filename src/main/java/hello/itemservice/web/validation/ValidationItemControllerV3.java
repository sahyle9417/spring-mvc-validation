package hello.itemservice.web.validation;

import hello.itemservice.domain.item.Item;
import hello.itemservice.domain.item.ItemRepository;
import hello.itemservice.domain.item.SaveCheck;
import hello.itemservice.domain.item.UpdateCheck;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/validation/v3/items")
@RequiredArgsConstructor
public class ValidationItemControllerV3 {

    private final ItemRepository itemRepository;

    @GetMapping
    public String items(Model model) {
        List<Item> items = itemRepository.findAll();
        model.addAttribute("items", items);
        return "validation/v3/items";
    }

    @GetMapping("/{itemId}")
    public String item(@PathVariable long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "validation/v3/item";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        model.addAttribute("item", new Item());
        return "validation/v3/addForm";
    }

    // ModelAttribute 어노테이션은 각 필드에 대해 타입 변환 시도해서 실패하면 typeMismatch 로 FieldError 추가
    // 타입 변환에 성공한 필드에 대해서 BeanValidator 가 검증 시도 (타입 변환 실패한 필드는 BeanValidator 검증 수행 X)
    // BeanValidator 는 Validated 또는 Valid 어노테이션 붙은 객체의 각 필드에 대해
    // 검증 어노테이션(NotNull, NotBlank, Range, Max 등)에 맞게 검증 수행하고 실패 시 BindingResult 에 등록
    // BeanValidation 에러 메시지 우선순위 : properties 파일 -> 검증 어노테이션 message 속성 -> 라이브러리 기본값
    // @PostMapping("/add")
    public String addItemV1(@Validated @ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        // FieldError 는 BeanValidator 와 어노테이션으로 쉽게 수행 가능하지만,
        // 아래와 같이 여러 Field 들을 종합적으로 보고 검증해야하는 ObjectError 는 사용자가 직접 아래와 같이 로직을 구현해야 함
        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
                // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
                // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
                // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
                bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));
            }
        }

        if (bindingResult.hasErrors()) {
            return "validation/v3/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v3/items/{itemId}";
    }

    // ModelAttribute 어노테이션은 각 필드에 대해 타입 변환 시도해서 실패하면 typeMismatch 로 FieldError 추가
    // 타입 변환에 성공한 필드에 대해서 BeanValidator 가 검증 시도 (타입 변환 실패한 필드는 BeanValidator 검증 수행 X)
    // BeanValidator 는 Validated 또는 Valid 어노테이션 붙은 객체의 각 필드에 대해
    // 검증 어노테이션(NotNull, NotBlank, Range, Max 등)에 맞게 검증 수행하고 실패 시 BindingResult 에 등록
    // BeanValidation 에러 메시지 우선순위 : properties 파일 -> 검증 어노테이션 message 속성 -> 라이브러리 기본값
    // SaveCheck group 의 검증 조건 적용 (현업에서는 검증 group 복잡해서 사용X, 별도 객체 사용하는 것이 일반적)
    @PostMapping("/add")
    public String addItemV2(@Validated(SaveCheck.class) @ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        // FieldError 는 BeanValidator 와 어노테이션으로 쉽게 수행 가능하지만,
        // 아래와 같이 여러 Field 들을 종합적으로 보고 검증해야하는 ObjectError 는 사용자가 직접 아래와 같이 로직을 구현해야 함
        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
                // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
                // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
                // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
                bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));
            }
        }

        if (bindingResult.hasErrors()) {
            return "validation/v3/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v3/items/{itemId}";
    }

    @GetMapping("/{itemId}/edit")
    public String editForm(@PathVariable Long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "validation/v3/editForm";
    }

    // @PostMapping("/{itemId}/edit")
    public String editV1(@PathVariable Long itemId, @Validated @ModelAttribute Item item, BindingResult bindingResult) {

        // FieldError 는 BeanValidator 와 어노테이션으로 쉽게 수행 가능하지만,
        // 아래와 같이 여러 Field 들을 종합적으로 보고 검증해야하는 ObjectError 는 사용자가 직접 아래와 같이 로직을 구현해야 함
        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
                // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
                // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
                // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
                bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));
            }
        }

        if (bindingResult.hasErrors()) {
            return "validation/v3/editForm";
        }

        itemRepository.update(itemId, item);
        return "redirect:/validation/v3/items/{itemId}";
    }

    // UpdateCheck group 의 검증 조건 적용 (현업에서는 검증 group 복잡해서 사용X, 별도 객체 사용하는 것이 일반적)
    @PostMapping("/{itemId}/edit")
    public String editV2(@PathVariable Long itemId, @Validated(UpdateCheck.class) @ModelAttribute Item item, BindingResult bindingResult) {

        // FieldError 는 BeanValidator 와 어노테이션으로 쉽게 수행 가능하지만,
        // 아래와 같이 여러 Field 들을 종합적으로 보고 검증해야하는 ObjectError 는 사용자가 직접 아래와 같이 로직을 구현해야 함
        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
                // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
                // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
                // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
                bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));
            }
        }

        if (bindingResult.hasErrors()) {
            return "validation/v3/editForm";
        }

        itemRepository.update(itemId, item);
        return "redirect:/validation/v3/items/{itemId}";
    }

}
