package hello.itemservice.web.validation;

import hello.itemservice.domain.item.Item;
import hello.itemservice.domain.item.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/validation/v2/items")
@RequiredArgsConstructor
public class ValidationItemControllerV2 {

    private final ItemRepository itemRepository;
    private final ItemValidator itemValidator;

    // 매 요청(controller 호출)마다 WebDataBinder 가 생성되어 파라미터 바인딩과 검증 수행
    // InitBinder 어노테이션 사용해서 WebDataBinder 에 Validator 추가해놓고 검증할 객체에 Validated 어노테이션 붙이면 검증 수행
    // 특정 객체에 대해 어떤 Validator 적용할지 판단하기 위해 Validator 클래스의 supports 함수를 사용함
    // 단, InitBinder 어노테이션은 해당 controller 에만 영향을 줌
    // 모든 controller 에 Validator 추가하려면 메인 클래스가 WebMvcConfigurer 를 구현하고 getValidator 함수 Override 하면 됨
    @InitBinder
    public void init(WebDataBinder dataBinder) {
        dataBinder.addValidators(itemValidator);
    }

    @GetMapping
    public String items(Model model) {
        List<Item> items = itemRepository.findAll();
        model.addAttribute("items", items);
        return "validation/v2/items";
    }

    @GetMapping("/{itemId}")
    public String item(@PathVariable long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "validation/v2/item";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        model.addAttribute("item", new Item());
        return "validation/v2/addForm";
    }

    // BindingResult 파라미터는 반드시 대상 ModelAttribute 객체 바로 뒤에 와야만 함
    // BindingResult 없을 땐 Binding 실패(e.g. 타입 오류)하면 400 오류 페이지로 이동 (컨트롤러 호출X)
    // BindingResult 있으면 Binding 실패(e.g. 타입 오류)해도 BindingResult 에 해당 오류를 Field Error 로 담고, 컨트롤러 호출
    // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
    // @PostMapping("/add")
    public String addItemV1(@ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        // 검증 로직
        if (!StringUtils.hasText(item.getItemName())) {
            bindingResult.addError(new FieldError("item", "itemName", "상품 이름은 필수입니다."));
        }
        if (item.getPrice() == null || item.getPrice() < 1000 || item.getPrice() > 1000000) {
            bindingResult.addError(new FieldError("item", "price", "가격은 1,000 ~ 1,000,000 까지 허용합니다."));
        }
        if (item.getQuantity() == null || item.getQuantity() >= 9999) {
            bindingResult.addError(new FieldError("item", "quantity", "수량은 최대 9,999 까지 허용합니다."));
        }
        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                bindingResult.addError(new ObjectError("item", "가격 * 수량의 합은 10,000원 이상이어야 합니다. 현재 값 = " + resultPrice));
            }
        }

        if (bindingResult.hasErrors()) {
            log.info("errors = {}", bindingResult);
            // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
            // model.addAttribute("bindingResult", bindingResult);
            return "validation/v2/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v2/items/{itemId}";
    }

    // BindingResult 파라미터는 반드시 대상 ModelAttribute 객체 바로 뒤에 와야만 함
    // BindingResult 없을 땐 Binding 실패(e.g. 타입 오류)하면 400 오류 페이지로 이동 (컨트롤러 호출X)
    // BindingResult 있으면 Binding 실패(e.g. 타입 오류)해도 BindingResult 에 해당 오류를 Field Error 로 담고, 컨트롤러 호출
    // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
    // @PostMapping("/add")
    public String addItemV2(@ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        // Binding 실패한 필드는 사용자 입력값을 객체에 저장할 수 없음 (e.g. Integer 자리에 String 값 입력받은 경우)
        // spring 은 Binding 실패한 필드에 대해 FieldError 객체 만들어서 BindingResult 에 넣어줌 (해당 필드값은 null 저장)

        // 검증 로직
        if (!StringUtils.hasText(item.getItemName())) {
            // FieldError(objectName, fieldName, defaultMessage)
            // bindingResult.addError(new FieldError("item", "itemName", "상품 이름은 필수입니다."));

            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // rejectedValue : 처리 실패한 사용자 입력값 (폼으로 되돌아가도 실패한 사용자 입력값을 입력 칸에 유지하기 위해 사용)
            // bindingFailure : 타입 오류 같은 바인딩 실패인지 여부 (바인딩은 성공하고 비즈니스 로직 검증 실패라면 이 값은 false)
            bindingResult.addError(new FieldError("item", "itemName", item.getItemName(), false, null, null, "상품 이름은 필수입니다."));
        }

        if (item.getPrice() == null || item.getPrice() < 1000 || item.getPrice() > 1000000) {
            // FieldError(objectName, fieldName, defaultMessage)
            // bindingResult.addError(new FieldError("item", "price", "가격은 1,000 ~ 1,000,000 까지 허용합니다."));

            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // rejectedValue : 처리 실패한 사용자 입력값 (폼으로 되돌아가도 실패한 사용자 입력값을 입력 칸에 유지하기 위해 사용)
            // bindingFailure : 타입 오류 같은 바인딩 실패인지 여부 (바인딩은 성공하고 비즈니스 로직 검증 실패라면 이 값은 false)
            bindingResult.addError(new FieldError("item", "price", item.getPrice(), false, null, null, "가격은 1,000 ~ 1,000,000 까지 허용합니다."));
        }

        if (item.getQuantity() == null || item.getQuantity() >= 9999) {
            // FieldError(objectName, fieldName, defaultMessage)
            // bindingResult.addError(new FieldError("item", "quantity", "수량은 최대 9,999 까지 허용합니다."));

            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // rejectedValue : 처리 실패한 사용자 입력값 (폼으로 되돌아가도 실패한 사용자 입력값을 입력 칸에 유지하기 위해 사용)
            // bindingFailure : 타입 오류 같은 바인딩 실패인지 여부 (바인딩은 성공하고 비즈니스 로직 검증 실패라면 이 값은 false)
            bindingResult.addError(new FieldError("item", "quantity", item.getQuantity(), false, null, null, "수량은 최대 9,999 까지 허용합니다."));
        }

        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, defaultMessage)
                // bindingResult.addError(new ObjectError("item", "가격 * 수량의 합은 10,000원 이상이어야 합니다. 현재 값 = " + resultPrice));

                // ObjectError(objectName, codes, arguments, defaultMessage)
                bindingResult.addError(new ObjectError("item", null, null, "가격 * 수량의 합은 10,000원 이상이어야 합니다. 현재 값 = " + resultPrice));
            }
        }

        if (bindingResult.hasErrors()) {
            log.info("errors = {}", bindingResult);
            // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
            // model.addAttribute("bindingResult", bindingResult);
            // thymeleaf 의 th:field 는 정상 상황에서 모델 객체의 값 사용, 오류 발생 시 FieldError 에서 보관한 값 사용
            return "validation/v2/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v2/items/{itemId}";
    }

    // BindingResult 파라미터는 반드시 대상 ModelAttribute 객체 바로 뒤에 와야만 함
    // BindingResult 없을 땐 Binding 실패(e.g. 타입 오류)하면 400 오류 페이지로 이동 (컨트롤러 호출X)
    // BindingResult 있으면 Binding 실패(e.g. 타입 오류)해도 BindingResult 에 해당 오류를 Field Error 로 담고, 컨트롤러 호출
    // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
    // @PostMapping("/add")
    public String addItemV3(@ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        // Binding 실패한 필드는 사용자 입력값을 객체에 저장할 수 없음 (e.g. Integer 자리에 String 값 입력받은 경우)
        // spring 은 Binding 실패한 필드에 대해 FieldError 객체 만들어서 BindingResult 에 넣어줌 (해당 필드값은 null 저장)

        // 검증 로직
        if (!StringUtils.hasText(item.getItemName())) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // rejectedValue : 처리 실패한 사용자 입력값 (폼으로 되돌아가도 실패한 사용자 입력값을 입력 칸에 유지하기 위해 사용)
            // bindingFailure : 타입 오류 같은 바인딩 실패인지 여부 (바인딩은 성공하고 비즈니스 로직 검증 실패라면 이 값은 false)
            // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
            // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
            // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
            // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
            bindingResult.addError(new FieldError("item", "itemName", item.getItemName(), false, new String[]{"required.item.itemName"}, null, null));
        }

        if (item.getPrice() == null || item.getPrice() < 1000 || item.getPrice() > 1000000) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // rejectedValue : 처리 실패한 사용자 입력값 (폼으로 되돌아가도 실패한 사용자 입력값을 입력 칸에 유지하기 위해 사용)
            // bindingFailure : 타입 오류 같은 바인딩 실패인지 여부 (바인딩은 성공하고 비즈니스 로직 검증 실패라면 이 값은 false)
            // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
            // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
            // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
            // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
            bindingResult.addError(new FieldError("item", "price", item.getPrice(), false, new String[]{"range.item.price"}, new Object[]{1000, 1000000}, null));
        }

        if (item.getQuantity() == null || item.getQuantity() >= 9999) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // rejectedValue : 처리 실패한 사용자 입력값 (폼으로 되돌아가도 실패한 사용자 입력값을 입력 칸에 유지하기 위해 사용)
            // bindingFailure : 타입 오류 같은 바인딩 실패인지 여부 (바인딩은 성공하고 비즈니스 로직 검증 실패라면 이 값은 false)
            // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
            // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
            // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
            // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
            bindingResult.addError(new FieldError("item", "quantity", item.getQuantity(), false, new String[]{"max.item.quantity"}, new Object[]{9999}, null));
        }

        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // codes : 에러 메시지 코드값(spring.messages.basename 에 정의한 properties 파일에 기록), 여러 개 넘기면 앞에서부터 검색
                // arguments : 에러 메시지가 사용할 argument (일관된 오류 메시지 사용 가능)
                // defaultMessage : codes 에 지정한 에러 메시지 코드값들이 모두 없다면 defaultMessage 출력
                // properties 에 에러 메시지 코드값들(codes)이 없고, defaultMessage 도 null 이면 400 오류 페이지로 이동
                bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));
            }
        }

        if (bindingResult.hasErrors()) {
            log.info("errors = {}", bindingResult);
            // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
            // model.addAttribute("bindingResult", bindingResult);
            // thymeleaf 의 th:field 는 정상 상황에서 모델 객체의 값 사용, 오류 발생 시 FieldError 에서 보관한 값 사용
            return "validation/v2/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v2/items/{itemId}";
    }

    // BindingResult 파라미터는 반드시 대상 ModelAttribute 객체 바로 뒤에 와야만 함
    // BindingResult 없을 땐 Binding 실패(e.g. 타입 오류)하면 400 오류 페이지로 이동 (컨트롤러 호출X)
    // BindingResult 있으면 Binding 실패(e.g. 타입 오류)해도 BindingResult 에 해당 오류를 Field Error 로 담고, 컨트롤러 호출
    // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
    // @PostMapping("/add")
    public String addItemV4(@ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {
        // Binding 실패한 필드는 사용자 입력값을 객체에 저장할 수 없음 (e.g. Integer 자리에 String 값 입력받은 경우)
        // spring 은 Binding 실패한 필드에 대해 FieldError 객체 만들어서 BindingResult 에 넣어줌 (해당 필드값은 null 저장)

        // 검증 로직
        if (!StringUtils.hasText(item.getItemName())) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // bindingResult.addError(new FieldError("item", "itemName", item.getItemName(), false, new String[]{"required.item.itemName"}, null, null));

            // rejectValue(fieldName, errorCode)
            // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
            // 필드 에러 메시지 코드 우선순위 (에러코드.객체명.필드명 -> 에러코드.필드명 -> 에러코드.필드타입 -> 에러코드)
            // FieldError 객체의 codes 배열에 MessageCodesResolver 가 반환한 필드 에러 메시지 코드들을 담아서 FieldError 객체 생성 후 이를 bindingResult 에 담는다.
            bindingResult.rejectValue("itemName", "required");
        }

        if (item.getPrice() == null || item.getPrice() < 1000 || item.getPrice() > 1000000) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // bindingResult.addError(new FieldError("item", "price", item.getPrice(), false, new String[]{"range.item.price"}, new Object[]{1000, 1000000}, null));

            // rejectValue(fieldName, errorCode, errorArgs, defaultMessage)
            // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
            // 필드 에러 메시지 코드 우선순위 (에러코드.객체명.필드명 -> 에러코드.필드명 -> 에러코드.필드타입 -> 에러코드)
            // FieldError 객체의 codes 배열에 MessageCodesResolver 가 반환한 필드 에러 메시지 코드들을 담아서 FieldError 객체 생성 후 이를 bindingResult 에 담는다.
            bindingResult.rejectValue("price", "range", new Object[]{1000, 1000000}, null);
        }

        if (item.getQuantity() == null || item.getQuantity() >= 9999) {
            // FieldError(objectName, fieldName, rejectedValue, bindingFailure, codes, arguments, defaultMessage)
            // bindingResult.addError(new FieldError("item", "quantity", item.getQuantity(), false, new String[]{"max.item.quantity"}, new Object[]{9999}, null));

            // rejectValue(fieldName, errorCode, errorArgs, defaultMessage)
            // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
            // 필드 에러 메시지 코드 우선순위 (에러코드.객체명.필드명 -> 에러코드.필드명 -> 에러코드.필드타입 -> 에러코드)
            // FieldError 객체의 codes 배열에 MessageCodesResolver 가 반환한 필드 에러 메시지 코드들을 담아서 FieldError 객체 생성 후 이를 bindingResult 에 담는다.
            bindingResult.rejectValue("quantity", "max", new Object[]{9999}, null);
        }

        if (item.getPrice() != null && item.getQuantity() != null) {
            int resultPrice = item.getPrice() * item.getQuantity();
            if (resultPrice < 10000) {
                // ObjectError(objectName, codes, arguments, defaultMessage)
                // bindingResult.addError(new ObjectError("item", new String[]{"totalPriceMin"}, new Object[]{10000, resultPrice}, null));

                // reject(errorCode, errorArgs, defaultMessage)
                // MessageCodesResolver 가 에러 코드를 에러 메시지 코드들로 변환(resolve)하고 가장 높은 우선순위의 에러 메시지 코드의 메시지를 가져온다.
                // 객체 에러 메시지 코드 우선순위 (에러코드.객체명 -> 에러코드)
                // ObjectError 객체의 codes 배열에 MessageCodesResolver 가 반환한 객체 에러 메시지 코드들을 담아서 ObjectError 객체 생성 후 이를 bindingResult 에 담는다.
                bindingResult.reject("totalPriceMin", new Object[]{10000, resultPrice}, null);
            }
        }

        if (bindingResult.hasErrors()) {
            log.info("errors = {}", bindingResult);
            // bindingResult 는 자동으로 model 에 등록되므로 뷰에서는 그냥 사용하면 됨
            // model.addAttribute("bindingResult", bindingResult);
            // thymeleaf 의 th:field 는 정상 상황에서 모델 객체의 값 사용, 오류 발생 시 FieldError 에서 보관한 값 사용
            return "validation/v2/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v2/items/{itemId}";
    }

    // @PostMapping("/add")
    public String addItemV5(@ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (itemValidator.supports(item.getClass())) {
            itemValidator.validate(item, bindingResult);
        }

        if (bindingResult.hasErrors()) {
            return "validation/v2/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v2/items/{itemId}";
    }

    // item 에 Validated 어노테이션 추가
    @PostMapping("/add")
    public String addItemV6(@Validated @ModelAttribute Item item, BindingResult bindingResult, RedirectAttributes redirectAttributes, Model model) {

        if (bindingResult.hasErrors()) {
            return "validation/v2/addForm";
        }

        Item savedItem = itemRepository.save(item);
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/validation/v2/items/{itemId}";
    }

    @GetMapping("/{itemId}/edit")
    public String editForm(@PathVariable Long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "validation/v2/editForm";
    }

    @PostMapping("/{itemId}/edit")
    public String edit(@PathVariable Long itemId, @ModelAttribute Item item) {
        itemRepository.update(itemId, item);
        return "redirect:/validation/v2/items/{itemId}";
    }

}
