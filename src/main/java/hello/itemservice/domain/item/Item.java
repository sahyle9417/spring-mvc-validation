package hello.itemservice.domain.item;

import lombok.Data;

// ModelAttribute 어노테이션은 각 필드에 대해 타입 변환 시도해서 실패하면 typeMismatch 로 FieldError 추가
// 타입 변환에 성공한 필드에 대해서 BeanValidator 가 검증 시도 (타입 변환 실패한 필드는 BeanValidator 검증 수행 X)
// BeanValidator 는 Validated 또는 Valid 어노테이션 붙은 객체의 각 필드에 대해
// 검증 어노테이션(NotNull, NotBlank, Range, Max 등)에 맞게 검증 수행하고 실패 시 BindingResult 에 등록
// BeanValidation 에러 메시지 우선순위 : properties 파일 -> 검증 어노테이션 message 속성 -> 라이브러리 기본값
// 검증 group 은 현업에서는 복잡도가 높아져서 거의 사용하지 않음 (별도 객체 사용하는 것이 일반적)
@Data
public class Item {

//    @NotNull(groups = UpdateCheck.class)
    private Long id;

//    @NotBlank(groups = {SaveCheck.class, UpdateCheck.class})
    private String itemName;

//    @NotNull(groups = {SaveCheck.class, UpdateCheck.class})
//    @Range(min = 1000, max = 1000000, groups = {SaveCheck.class, UpdateCheck.class})
    private Integer price;

//    @NotNull(groups = {SaveCheck.class, UpdateCheck.class})
//    @Max(value = 9999, groups = {SaveCheck.class})
    private Integer quantity;

    public Item() {
    }

    public Item(String itemName, Integer price, Integer quantity) {
        this.itemName = itemName;
        this.price = price;
        this.quantity = quantity;
    }
}
