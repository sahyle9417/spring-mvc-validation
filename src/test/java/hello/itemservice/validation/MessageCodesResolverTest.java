package hello.itemservice.validation;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DefaultMessageCodesResolver;
import org.springframework.validation.MessageCodesResolver;

public class MessageCodesResolverTest {

    MessageCodesResolver codesResolver = new DefaultMessageCodesResolver();

    @Test
    void messageCodesResolverObject() {
        String[] messageCodes = codesResolver.resolveMessageCodes("required", "item");
        // 객체 에러 메시지 코드 생성 규칙
        Assertions.assertThat(messageCodes).containsExactly(
                "required.item",    // 에러코드.객체명
                "required"          // 에러코드
        );
    }

    @Test
    void messageCodesResolverField() {
        String[] messageCodes = codesResolver.resolveMessageCodes("required", "item", "itemName", String.class);
        // 필드 에러 메시지 코드 생성 규칙
        Assertions.assertThat(messageCodes).containsExactly(
                "required.item.itemName",       // 에러코드.객체명.필드명
                "required.itemName",            // 에러코드.필드명
                "required.java.lang.String",    // 에러코드.필드타입
                "required"                      // 에러코드
        );
    }

}
